# Wiki Online Operated by MQD

[pripadať](.gitbook/assets/view) - có vẻ, dường như

&#x20;prípadne zákaznícku podporu - có thể hỗ trợ khách hàngpripadať \[pripadac] impf có vẻ, dường như, (?có thể)

minhquy minhquy\


***

[nepovolený](.gitbook/assets/view) - không được phép

nepovolený \[ɲεpovolεniː] adj không được phépdochádzkapovinná školská dochádzka học vấn bắt buộc do nhà nước quy địnhpovinnýbyť povinný urobiť čo bắt buộc làm gìbắt buộckhông bắt buộc nepovinný, nezáväzný, nezáväzne . . .\


minhquy minhquy\


***

[nepovinný](.gitbook/assets/view) - không bắt buộc, tùy ý

nepovinný \[ɲεpovinːiː] adj không bắt buộc, tùy ýdochádzkapovinná školská dochádzka học vấn bắt buộc do nhà nước quy địnhpovinnýbyť povinný urobiť čo bắt buộc làm gìbắt buộckhông bắt buộc nepovinný, nezáväzný, nezáväzne

minhquy minhquy\


***

[statický](.gitbook/assets/view) - tĩnh (điện v.v.)

statický \[statitskiː] adj tĩnh (điện v.v.)

minhquy minhquy\


***

[akokoľvek1](.gitbook/assets/view) - thế nào cũng được, cách nào cũng được

akokoľvek1 \[akokoʎvεk] pron thế nào cũng được, cách nào cũng được

minhquy minhquy\


***

[akoby](.gitbook/assets/view) - cứ như là, như thể

akoby \[akobi] conj cứ như là, như thể ibaiba ak (by) trừ phi, trừ khi, nếu không

minhquy minhquy\


***

[akcia(1)](.gitbook/assets/view) - sự khuyết mãi, hành động, hoạt động (akcia(2 )cổ phần)

akcia1 \[aktsia] f1.sự hoạt động2.sự khuyến mại3.nhiệm vụ (rủi ro)4.hành động (bóng đá)&#x20;

minhquy minhquy\


***

[akceptovať](.gitbook/assets/view) - chấp nhận

akceptovať \[aktsεptovac] impf/perf chấp nhận cái gìibaiba ak (by) trừ phi, trừ khi, nếu khôngpovedaťak sa to tak dá povedať để tôi nóitrebaak bude treba nếu cầnbohAk boh dá. Nếu trời cho.mýliť saAk sa nemýlim... Nế . . .\


minhquy minhquy\


***

[abdikovať](.gitbook/assets/view) - từ bỏ ( địa vị, .... )

&#x20;abdikovať \[abdikovac] perf từ bỏ (địa vị v.v.)

minhquy minhquy\


***

[podmet](.gitbook/assets/view) - chủ ngữ

podmet \[podmεt] m (Ling) chủ ngữpredmet \[prεdmεt] m1.vật2.chủ đề cái gì3.môn học4.(Ling) tân ngữ (ngôn ngữ học) . . .\


minhquy minhquy\


***

[prísudok](.gitbook/assets/view) - vị ngữ

\<p>\<br>prísudok \[priːsudok] m\<br>(Ling) vị ngữ\</p>

minhquy\


***

[Aktívne particípiá](.gitbook/assets/view) - ?phân từ chủ động

aktívne \[aktiːvɲε] adv tích cực, hăng hái

minhquy minhquy\


***

[Pasívny particípiá](.gitbook/assets/view) - ?phân từ bị động

particípium \[partitsiːp̑um] n (Ling) phân từpasívny \[pasiːvni] adj tiêu cực, bị động (thái độ v.v.

minhquy minhquy\


***

[vyžiadať (si)](.gitbook/assets/view) - yêu cầu

&#x20;vyžiadať si perf1.yêu cầu 2.đòi cái gì (bồi thương v.v.) 3.đòi cái gì (thời gian) 4.đòi hỏi phr  Vyžiada si to nejaký čas. . . .\


minhquy minhquy\


***

[encyklopédia ](.gitbook/assets/view)- bách khoa toàn thư

encyklopédia -ie ž. náučné dielo obsahujúce . . .\


minhquy minhquy\


***

[rôzny](.gitbook/assets/view) - đa dạng, khác nhau

rôzny \[rؑozni] adj1.khác nhau2.đa dạngkháckhác nhau rozdielne, rozdielny, rôzny, rozličný

minhquy minhquy\


***

[evidovať](.gitbook/assets/view) - ghi vào sổ

evidovať \[εvidovac] impf ghi vào sổ điều gì&#x20;

minhquy minhquy\


***

[pravidelne](.gitbook/assets/view) - thường xuyên, đều đặn

pravidelne \[praviɟεlɲε] adv. thường xuyên, đều đặnpravidelnosť \[praviɟεlnosc] f  sự đều đặn

minhquy minhquy\


***

[medzitým](.gitbook/assets/view) - vào thời gian giữa

medzitým \[mεdzitiːm] adv vào thời gian giữa

minhquy minhquy\


***

[potok](.gitbook/assets/view) - suối\
minhquy minhquy\


***
