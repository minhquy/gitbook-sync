# Wiki Online Operated by MQD

***

[Ký hiệu " & " trong PHP](.gitbook/assets/view) - Là môt lập trình viên PHP (có thể cả ngôn ngữ khác) chắc các bạn không xa lại gì với ký hiệu &. Nhưng bạn có chắc là đã sử dụng hết tình năng của nó không? Vậy cùng điểm lại nhé.

Toán tử thao tác bit AND(AND Bitwise Operators)Toán tử thao tác bit AND lấy 2 toán hạng nhị nhân có chiều dài bằng nhau và thực hiện phép toán lý luận AND trên mỗi cặp bit tương ứng bằng cách nhân chúng lại với nhau. Nhờ đó, nếu cả 2 bit ở vị trí được so sánh đều là 1, thì bit hiển thị ở dạng nhị phân sẽ là 1 (1 x 1 = 1); ngược lại thì kết quả sẽ là 0 (1 x 0 = 0)Trong php và nhiều ngôn ngữ khác, & là toán tử thao tác bit AND.14 = 1110 . . .\


minhquy\


***

[Regular Expression PHP](.gitbook/assets/view) - là gì?

Regular Expression là một chương trình mẫu (biểu thức chính quy) dùng để so khớp với dữ liệu. Ngoài cách ghi này ra thì người ta cò ghi tắt là ReExp. Regular Expression hay còn gọi là biểu thức chính quy được dùng để xử lý chuỗi nâng cao thông qua biểu thức riêng của nó, những biểu thức này sẽ có những nguyên tắc riêng và bạn phải tuân theo nguyên tắc đó thì biểu thức của bạn mới hoạt động được. Ngoài cái tên gọi Regular E . . .\


88.212.54.57 minhquy\


***

[wordwrap()](.gitbook/assets/view) - hàm wordwrap trong PHP

Hàm wordwrap() sẽ thêm một kí tự hoặc một thẻ nào đó vào chuỗi khi đã đếm đủ số kí tự nhất định. Ví dụ cứ sau mỗi 20 kí tự ta sẽ xuống dòng, trong trường hợp này ta sẽ sử dụng hàm wordwrap() để xử lý.Cú phápCú pháp: wordwrap( $str, $width, $break, $cut);Trong đó:$str là chuỗi cần xử lý..$width là độ dài mà cứ sau $width kí tự sẽ thêm $break vào chuỗi $str.$cut là tham số. Mặc . . .\


minhquy minhquy\


***

[htmlentities()](.gitbook/assets/view) - Hàm htmlentities() sẽ chuyển các kí tự thích hợp thành các kí tự HTML entiies.

Cú phápCú pháp: htmlentities( $str, $flags = ENT\_COMPAT | ENT\_HTML401, $encoding = ini\_get("default\_charset");Trong đó:$str là chuỗi truyền vào.$flags là tham số mang một trong những giá trị sau:ENT\_COMPAT.ENT\_QUOTES.ENT\_NOQUOTES.ENT\_IGNORE .ENT\_SUBSTITUTE.ENT\_DISALLOWED.ENT\_HTML401.ENT\_XML1. . . .\


minhquy minhquy\


***

[custom from edit.php - htmlspecialchar](.gitbook/assets/view) - custom form edit.php - htmlspecialchar

88.212.54.57 minhquy\


***

[\_\_construct()](.gitbook/assets/view) - trong PHP

Trong mỗi lớp có hai hàm rất đặc biệt đó là hàm khởi tạo và hàm hủy. Hàm khởi tạo sẽ được tự động gọi khi bạn khởi tạo mới một đối tượng, còn hàm hủy thì sẽ được gọi khi đối tượng bị hủy. // PHP cung cấp một hàm đặc biệt được gọi là \_\_construct() để định nghĩa một constructor. Bạn có thể truyền bao nhiêu tham số tùy bạn vào trong hàm constructor này. Ví dụ sau sẽ tạo một constructor cho lớp Books và nó sẽ khởi tạo price và title c . . .\


88.212.54.57 minhquy\


***

[CSRF](.gitbook/assets/view) - Cross-site Request Forgery

“XSS & CSRF Những người anh em” là kỹ thuật tấn công giả mạo chính chủ thể của nó. cách chống CSRF - Sử dụng token - Sử dụng cookie riêng biệt cho trang quản trị - Kiểm tra REFERRER - Kiểm tra IP

minhquy minhquy\


***

[mysql\_real\_escape\_string()](.gitbook/assets/view) - chuyển một chuỗi thành chuỗi query an toàn\
88.212.54.57 minhquy\


***

[trim( $str, $char);](.gitbook/assets/view) - hàm loại bỏ khoảng trắng

Hàm trim() sẽ loại bỏ khoảng trắng( hoặc bất kì kí tự nào được cung cấp) dư thừa ở đầu và cuối chuỗi. $str là chuỗi cần loại bỏ các kí tự. $char là tham số không bắt buộc quy định các kí tự sẽ bị loại bỏ ở đầu và cuối chuỗi. Nếu không truyền, hàm trim() sẽ loại bỏ khoảng trắng.

88.212.54.57 minhquy\


***

[strip\_tags()](.gitbook/assets/view) - trong PHP

Hàm strip\_tags() sẽ loại bỏ các thẻ HTML và PHP ra khỏi chuỗi. Hàm sẽ trả về chuỗi đã loại bỏ hết các thẻ HTML và PHP.

minhquy minhquy\


***

[string](.gitbook/assets/view) - chuỗi

Được dịch từ tiếng Anh-Trong lập trình máy tính, một chuỗi theo truyền thống là một chuỗi các ký tự, dưới dạng hằng số theo nghĩa đen hoặc là một loại biến. Cái sau có thể cho phép các phần tử của nó bị đột biến và độ dài thay đổi, hoặc nó có thể được sửa.

minhquy minhquy\


***

[PHP ERROR REPORT](.gitbook/assets/view) - đoạn PHP báo lỗi [\[email protected\]](<.gitbook/assets/email protection>)

if($insert\_query){ echo "\<script> alert('Bình luận đã được thêm vào!'); window.location.href = 'index.php'; \</script>"; exit; }else{ echo "\<h3>Lỗi gì rồi.. \<a href='mailto:[\[email protected\]](<.gitbook/assets/email protection>)\&subject=Báo Lỗi Website [\[email protected\]](<.gitbook/assets/email protection>)'>Báo lỗi\</a>\</h3>"; }

minhquy minhquy\


***

[PHP Data Types](.gitbook/assets/view) - Các kiểu dữ liệu PHP

Các biến có thể lưu trữ dữ liệu thuộc các kiểu khác nhau và các kiểu dữ liệu khác nhau có thể làm những việc khác nhau. PHP hỗ trợ các kiểu dữ liệu sau: 1. String 2. Integer 3. Float (floating point numbers - also called double) 4. Boolean true/false 5. Array - Mảng liên kết - - 6. Object đối tượng 7. NULL 8. Resource

minhquy\


***
