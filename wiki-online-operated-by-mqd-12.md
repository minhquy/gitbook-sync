# Wiki Online Operated by MQD

[ERP](.gitbook/assets/view) - (viết tắt của Enterprise Resource Planning) là hệ thống hoạch định nguồn lực doanh nghiệp. Hiểu đơn giản thì hệ thống ERP là một phần mềm thống nhất, đa chức năng liên kết mọi hoạt động của doanh nghiệp, từ quản trị toàn diện đầu vào, đầu ra; tới lập kế hoạch, thống kê, kiểm soát các nghiệp vụ về sản xuất, tài chính, nhân sự… Bên cạnh đó, ERP còn hỗ trợ cung cấp báo cáo phân tích chuyên sâu và đưa ra các dự báo, giúp cho nhà quản lý hoặc các bộ phận tác nghiệp hiệu quả.

Phần mềm ERP hiện tại đang là điểm sáng trong cuộc chuyển đổi số dành cho doanh nghiệp sản xuất, kinh doanh. Lợi ích dành cho doanh nghiệp khi hệ thống ERP được triển khai thành công là: quản trị doanh nghiệp toàn diện, xem báo cáo và phê duyệt online theo thời gian thực, các bộ phận phối hợp công việc ăn ý, giảm 70% các quy trình làm việc thủ công, đưa ra quyết định kinh doanh kịp thời, sáng suốt…&#x20;

admin admin\
\


***

[sandbox](.gitbook/assets/view) - Trong bảo mật máy tính, sandbox là một cơ chế bảo mật để tách các chương trình đang chạy, thường nhằm giảm thiểu các lỗi hệ thống và / hoặc các lỗ hổng phần mềm lây lan.\
admin admin\
\


***

[Screaming Frog SEO Spider](.gitbook/assets/view) - là một ứng dụng bạn có thể cài đặt trên máy tính PC (Window, MAC, Linux). Nó cho bạn quét các liên kết, hình ảnh, CSS.. của một website. Về cơ bản phần mềm sẽ cho bạn biết các máy tìm kiếm sẽ nạp thông tin có trên website của bạn như thế nào. Với Thông tin này bạn có thể phân tích và đánh giá một website từ kía cạnh SEO onsite. Nó có thể tiết kiệm rất nhiều thời gian, các công việc cần làm bởi lẽ việc phân tích từng trang của một website lớn sẽ là thử thách với bạn.

https://www.screamingfrog.co.uk/seo-spider/

admin admin\
\


***

[ecwid.com](.gitbook/assets/view) - Bắt đầu bán Trực tuyến miễn phí

Chúng tôi đang đưa vào thử nghiệm miễn phí. Với Ecwid, bạn nhận được miễn phí MÃI MÃI. Thiết lập tài khoản miễn phí của bạn một lần và giữ nó bao lâu tùy thích. Nghiêm túc.https://www.ecwid.com/

minhquy admin\
\


***

[dodavvatel.sk](.gitbook/assets/view) - Công cụ tìm kiếm dodavatel.sk là một công cụ chuyên nghiệp hiệu quả cho ngày càng nhiều công ty dẫn đầu trong các lĩnh vực này không chỉ ở Slovakia và Cộng hòa Séc mà còn ở nước ngoài.

https://www.dodavatel.sk/https://www.dodavatel.sk/sk/c/internet/1702/https://www.dodavatel.sk/sk/c/softvr/1703/Công cụ tìm kiếm dodavatel.sk là một công cụ hữu hiệu cho những ai đang tìm kiếm các nhà cung cấp sản phẩm hoặc dịch vụ mới cho công ty của họ. Kể từ năm 1995, nó đã tạo dựng được vị thế trên thị trường Séc v . . .\


minhquy minhquy\
\


***

[tulu.sk](.gitbook/assets/view) - Công cụ trực tuyến tulu là một công cụ trợ giúp hữu ích trong việc tạo tài liệu hợp đồng cho nhân viên. Nó tự động chèn dữ liệu vào đúng vị trí trong các mẫu tài liệu và tạo các hợp đồng, xác nhận hoặc bổ sung được tạo sẵn cho một hoặc nhiều nhân viên cùng một lúc với một vài cú nhấp chuột. bạn có thể thử nó miễn phí.

http://www.tulu.sk/

minhquy minhquy\
\


***

[najzamestnavatel.sk](.gitbook/assets/view) - Kể từ năm 2012, đã công bố cuộc khảo sát Nhà tuyển dụng hấp dẫn nhất. Hàng năm, hơn 150 công ty từ nhiều ngành khác nhau nộp đơn xin bình chọn của công chúng. Người được hỏi chọn công ty mà họ muốn làm việc trong số những người được đề cử.

Profesia od roku 2012 vyhlasuje anketu Najatraktívnejší zamestnávateľ. O hlasy širokej verejnosti sa každoročne uchádza viac ako 150 spoločností z rôznych odvetví. Respondenti spomedzi nominovaných vyberajú spoločnosť, v ktorej by pracovali najradšej.

minhquy minhquy\
\


***

[mark.profesia.sk](.gitbook/assets/view) - HR aplikácia mark là một giải pháp hữu hiệu để quản lý trực tuyến các ứng viên trong đấu thầu. Công cụ này rõ ràng, trực quan, giúp cải thiện hình ảnh của công ty và nhờ đó, ứng viên tốt nhất chắc chắn sẽ không bị mất trong số lượng ứng viên.

HR aplikácia mark je efektívne riešenie pre online spravovanie uchádzačov vo výberových konaniach. Nástroj je prehľadný, intuitívny, pomáha zlepšovať imidž spoločnosti a vďaka nemu sa v kvantách uchádzačov ten najlepší kandidát určite nestratí.

minhquy minhquy\
\


***

[pozicie.sk](.gitbook/assets/view) - Danh mục các vị trí phổ biến nhất ở Slovakia chứa hàng trăm ngành nghề. Nó cung cấp thông tin chi tiết về các công việc cá nhân để giúp bạn lựa chọn nghề nghiệp tương lai của mình. Tại đây, bạn sẽ tìm thấy từ vựng về thị trường lao động và danh sách các khóa học và đào tạo chuyên nghiệp.

pozicie.sk

minhquy minhquy\
\


***

[platy.sk](.gitbook/assets/view) - Cổng thông tin Platy.sk cung cấp các phân tích tiền lương cá nhân cho mọi người hoặc một công cụ chuyên nghiệp để đặt mức lương tối ưu cho một vị trí. Nó cho phép bạn so sánh lương hoặc tính lương ròng miễn phí. Cuộc khảo sát của Slovakia dựa trên cuộc khảo sát lương chuyên nghiệp quốc tế Paylab tại hơn 20 quốc gia trên thế giới.

www.platy.sk

minhquy admin\
\


***

[edujobs.sk](.gitbook/assets/view) - Cổng thông tin Edujobs được tạo ra như một sáng kiến ​​nhằm đơn giản hóa các quy trình lựa chọn trong giáo dục. Nó đã nhận được giải thưởng Via Bona Slovakia vì sự minh bạch ngày càng tăng. Nó cho phép các trường học và cơ sở trường học quảng cáo miễn phí bất kỳ vị trí tuyển dụng nào.

\<p>\<a href="www.edujobs.sk">www.edujobs.sk\</a>\</p>

minhquy\
\


***

[profesia.sk](.gitbook/assets/view) - Cổng thông tin việc làm lớn nhất

&#x20;Pracovný portál Profesia spája zamestnávateľov s najväčšou databázou uchádzačov o prácu. Denne tu nájdete tisíce overených pracovných ponúk zo Slovenska aj zo zahraničia. Stránka ponúka užitočné informácie o trhu práce a množstvo ďalších služieb.Cổng thông tin việc làm Profesia kết nối các nhà tuyển dụng với cơ sở dữ liệu lớn nhất về những người tìm việc. Mỗi ngày, bạn sẽ tìm thấy hàng ngàn lời mời làm việc đã được xác minh từ Slovakia và nước ngo . . .\


minhquy minhquy\
\


***

[Greasemonkey](.gitbook/assets/view) - Tùy chỉnh cách một trang web hiển thị hoặc hoạt động, bằng cách sử dụng các đoạn JavaScript nhỏ.\
minhquy\
\


***

[Userscript (hoặc User script)](.gitbook/assets/view) - là các script dùng trong trình duyệt, hướng tới việc đọc thông tin của trang web hoặc thay đổi chúng.

&#x20;Về cơ bản, Userscript là JavaScript, được bổ sung một số API mạnh mẽ mà thông thường không được phép.Userscript thường không được phép cài trực tiếp vào trình duyệt, mà phải chạy thông qua một tiện ích (extension) quản lý, nếu không trình duyệt sẽ xem nó như một tệp JavaScript thông thường. Các trình duyệt nhân Chromium cho phép nó chạy như một tiện ích, tuy nhiên có nhiều hạn chế.Tùy thuộc vào tiện ích quản lý mà Userscript có đuôi mở rộng khác nha . . .\


minhquy minhquy\
\


***

[Tampermonkey chrome extension](.gitbook/assets/view) - là một trong những tiện ích mở rộng trình duyệt phổ biến nhất với hơn 10 triệu người dùng. Tampermonkey được sử dụng để chạy cái gọi là usercripts (đôi khi còn được gọi là Greasemonkey script) trên các trang web.\
minhquy\
\


***

[stackbit.com](.gitbook/assets/view) - Xây dựng Web Hiện đại

\<blockquote>\<p>Cách chúng tôi tạo và thiết kế trang web đang thay đổi nhờ sự phát triển không ngừng của các công cụ, phương pháp hay nhất và nhu cầu. Chúng đang được tạo tĩnh, phân phát từ CDN, được chỉnh sửa bằng Headless CMS và kết nối với vô số quy trình công việc của nhà phát triển.\</p>\<p>Những ý tưởng mới như Jamstack, máy tính không máy chủ và quy trình làm việc dựa trên git đang tạo ra một trang web hiệu quả và an toàn hơn. Chúng tôi tin rằng nh . . .\


minhquy\
\


***

[Jira (atlassian/software)](.gitbook/assets/view) - là một ứng dụng theo dõi và quản lý lỗi, vấn đề và dự án, được phát triển để làm quy trình này trở nên dễ dàng hơn cho mọi tổ chức. JIRA đã được thiết kế với trọng tâm vào kết quả công việc, có thể sử dụng ngay và linh hoạt khi sử dụng.

Links:https://www.atlassian.com/software/jirahttps://minhquy.atlassian.net/plugins/servlet/ac/com.atlassian.jira.emcee/enduser-discover#!/discover/app/zoom-jira-connector?source=quickstart\&installDialogOpen=true https://minhquy.atlassian.net/jira/soft . . .\


minhquy minhquy\
\


***

[mapbox.com](.gitbook/assets/view) - là gì?

Mapbox là một nền tảng cung cấp các dịch vụ lập bản đồ thông qua các API của họ với giá cả rất hợp lý và phần tốt nhất là họ không yêu cầu thông tin thanh toán nếu bạn đang sử dụng cấp miễn phí hào phóng của họ Chúng tôi đang xây dựng nền tảng vị trí trực tiếp cho một tương lai siêu kết nối và tự động. Vị trí là hệ thống tọa độ cho mọi thứ và mạng lưới cảm biến phi tập trung của chúng tôi biến dữ liệu trực tiếp th� . . .\


minhquy minhquy\
\


***

[Dropshipping](.gitbook/assets/view) - là phương pháp thực hiện bán lẻ mà một cửa hàng không lưu giữ sản phẩm được bán trong kho của mình. Thay vào đó, khi một cửa hàng bán một sản phẩm cụ thể, không có sản phẩm lưu kho mà họ mua sản phẩm từ một bên thứ 3 và vận chuyển trực tiếp cho khách hàng. Kết quả là, những người bán hàng đó không bao giờ nhìn thấy sản phẩm hoặc xử lý sản phẩm.\
minhquy\
\


***

[a0.to](.gitbook/assets/view) - nền tảng xác thực & ủy quyền ngoại vi cho bất kỳ ứng dụng nào - với khả năng mở rộng phù hợp với nhu cầu của bạn.

https://auth0.com/developers https://auth0.com/docs

minhquy minhquy\
\


***
